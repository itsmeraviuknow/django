FROM python:3.5-slim

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        postgresql-client \
    && rm -rf /var/lib/apt/lists/*

COPY requirements.txt ./
RUN pip install -r requirements.txt
COPY . .

EXPOSE 8000
RUN django-admin startproject mysite

WORKDIR /usr/src/app

CMD ["python", "/mysite/manage.py", "runserver", "0.0.0.0:8000"]
